//---------------------
// JS for Page Traversal
//---------------------

let welcomePage = document.getElementById('welcome-page');
let instructionsPage = document.getElementById('instructions-page');
let userPage = document.getElementById('user-page');
let gamePage = document.getElementById('game-page');
let instructionsBtn = document.getElementById('instructionsBtn');
let welcomePageStartBtn = document.getElementById('startBtn-welcome');
let userPageStartBtn = document.getElementById('startBtn-user');
let backBtns = document.querySelectorAll('[data-id="backBtn"]');

welcomePageStartBtn.addEventListener('click', () => {
  userPage.classList.remove('d-none');
  welcomePage.classList.add('d-none');
});

userPageStartBtn.addEventListener('click', () => {
  gamePage.classList.remove('d-none');
  userPage.classList.add('d-none');
});

instructionsBtn.addEventListener('click', () => {
  instructionsPage.classList.remove('d-none');
  welcomePage.classList.add('d-none');
});

backBtns.forEach((btn) => {
  btn.addEventListener('click', () => {
    btn.parentElement.parentElement.classList.add('d-none');
  });
});

backBtns[0].addEventListener('click', () => {
  welcomePage.classList.remove('d-none');
});

backBtns[1].addEventListener('click', () => {
  welcomePage.classList.remove('d-none');
});

backBtns[2].addEventListener('click', () => {
  userPage.classList.remove('d-none');
})
